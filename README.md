
# SonarQube

SonarQube is an automatic code review tool to detect bugs, vulnerabilities, and code smells in your code. It can integrate with your existing workflow to enable continuous code inspection across your project branches and pull requests.



## Installation

Install my-project with npm

```bash
  npm install my-project
  cd my-project
```
    
